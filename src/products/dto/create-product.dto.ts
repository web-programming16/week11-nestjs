import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(8, 20)
  name: string;

  @IsNotEmpty()
  price: number;
}
